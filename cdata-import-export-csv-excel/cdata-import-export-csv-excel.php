<?php
/*
Plugin Name: Import / Export customer data to csv / excel format
Plugin URI: /
Domain Path: /languages
Text Domain: cdata-import-export-csv-excel
Description: This plugin allows You to save customer personal data for presentation some product and save customer data on database and in the past show it on admin page or export to csv or excel format
Author: Gistolovskiy Aleksandr
Version: 1.0
Author URI: #

*/

/*  Copyright 2019 Gistolovskiy Aleksandr  (email: webdesign345@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if(!defined( 'ABSPATH' )) exit;
global $cdata_plugin;
$cdata_plugin = plugin_basename(__FILE__);

define("CUSTOMER_DATA_TO_CSV_EXCEL_URL", plugin_dir_url(__FILE__));
define("CDATA_TO_CSV_EXCEL_T_DOMAIN", "cdata-import-export-csv-excel");
$plugin_dir = plugin_dir_path( __FILE__ );

global $cdata_to_csv_excel_user_data_db_version;
$cdata_to_csv_excel_user_data_db_version = '1.0';

if( !function_exists('cdata_to_csv_excel_db_install') ) {
    function cdata_to_csv_excel_db_install(){
        global $wpdb;
        global $cdata_to_csv_excel_user_data_db_version;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table_name = $wpdb->prefix . "cdata_to_csv_excel";

        $sql = "CREATE TABLE {$table_name} (
          cdata_id BIGINT(9) unsigned NOT NULL AUTO_INCREMENT,
          cdata_fio VARCHAR(100) NOT NULL default '',
          cdata_phone VARCHAR(20) NOT NULL default '',
          cdata_email VARCHAR(50) NOT NULL default '',
          cdata_date VARCHAR(10) NOT NULL default '',
          cdata_time VARCHAR(10) NOT NULL default '',
          PRIMARY KEY  (cdata_fio),
          KEY cdata_id (cdata_id)
        );";

        dbDelta($sql);

        // Add default data
        do_action('cdata_insert_default_data');

        add_option('cdata_to_csv_excel_user_data_db_version', $cdata_to_csv_excel_user_data_db_version);
    }
}

// Register all plugin data
register_activation_hook(__FILE__, 'cdata_to_csv_excel_db_install');

if( !function_exists('cdata_to_csv_excel_data_delete')) {
    function cdata_to_csv_excel_data_delete(){
        cdata_to_csv_excel_db_uninstall();

        delete_option("cdata_to_csv_excel_user_data_db_version");
        delete_option("cdata_main_settings");
        delete_option("cdata_form_settings");
    }
}

// Remove all data when the plugin deleted
register_uninstall_hook(__FILE__, 'cdata_to_csv_excel_data_delete');

if( !function_exists('cdata_to_csv_excel_db_uninstall') ) {
    function cdata_to_csv_excel_db_uninstall() {
        global $wpdb;

        $table_name = $wpdb->prefix . "cdata_to_csv_excel";
        $wpdb->query("DROP TABLE IF EXISTS {$table_name}");

    }
}

// Add possibility for translate plugin to other language
add_action('plugins_loaded', 'cdata_to_csv_excel_localize');
function cdata_to_csv_excel_localize(){
    global $cdata_plugin;

    load_plugin_textdomain(CDATA_TO_CSV_EXCEL_T_DOMAIN, false, dirname( $cdata_plugin ) . '/languages');
}

/**
 * Plugin admin menu
 * Create plugin menu
 */
require_once('includes/class-cdata-menu.php');

//Main controller
require_once('includes/class-cdata-controller.php');

//Works with database
require_once('includes/class-cdata-model.php');

/**
 * Plugin admin menu
 * Create main plugin page with all items in the table
 */
require_once('includes/class-cdata-shortcode.php');

// TODO: Активация перевода
