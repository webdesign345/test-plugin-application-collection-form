=== Plugin Name ===
Contributors: Gistolovskiy Aleksandr
Donate link: /
Tags: Form for save customers for presentation product with save customers data and show it on admin page or export in csv or excel
Requires at least: 2.5
Tested up to: 5.4.1
Stable tag:/trunk
Last updated time:29.05.2020
Creation time:29.05.2020
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allow you to save customer personal data for presentation some product

== Description ==
This plugin allows You to save customer personal data for presentation some product and save customer data on database and in the past show it on admin page or export to csv or excel format

== Installation ==

1. Upload the plugin files to the `wp-content/plugins/import-export-csv-excel` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress

== Frequently Asked Questions ==

== Screenshots ==

1. No screenshots yet.

== Changelog ==

= 1.0 =
* Edit readme file

== Upgrade Notice ==

= 1.0 =
* No one here