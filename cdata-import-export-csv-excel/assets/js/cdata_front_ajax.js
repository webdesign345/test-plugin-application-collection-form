/*
 * Front AJAX functional for default save data from customer
 */

function saveCustomerData( param ) {
    jQuery.ajax({
        url: cdata_ajax_object.url,
        type: 'POST',
        dataType: 'json',
        data: {
            'action': 'saveCustomerData',
            'nonce': cdata_ajax_object.nonce,
            'fio': param.fio,
            'phone': param.phone,
            'email': param.email,
            'date': param.date,
            'time': param.time
        },
        success: function( data ) {
            //console.log('success');
            //console.log('data - ', data);

            var responce = data.data;

            if( false !== responce.responce ) {
                jQuery('body').trigger('successSaveCustomerData', responce );
            } else {
                jQuery('body').trigger('errorSaveCustomerData', responce );
            }
        },
        error: function( data ) {
            //console.log('error');

            var responce = data.data;

            jQuery('body').trigger('connectionErrorSaveCustomerData', responce );
        }
    })
}