jQuery(document).ready(function($) {

    if( $('#cdata_form').length != 0 ) {
        var form = $('#cdata_form'),
            submit_form = form.find('#cdata_submit'),
            cdata_fio = form.find('#cdata_fio'),
            cdata_tel = form.find('#cdata_tel'),
            cdata_email = form.find('#cdata_email'),
            cdata_date = form.find('#cdata_date'),
            cdata_time = form.find('#cdata_time'),
            fields = form.find('input').not('[type="submit"]');

        // Add validate functionality to fields
        validateField( {'elem':cdata_fio, 'size':100, 'reg':/[^а-яА-Я\s]/i} );
        validateField( {'elem':cdata_email, 'size':50, 'reg':/[^a-zA-Z0-9\@\.\-\_]/i} );

        // Add mask for phone field
        cdata_tel.mask('+7(999)-999-99-99', {completed : function(){
            checkFormFields( fields );
        } } );

        // Check validate for all fields
        cdata_fio.on('focusout', function() {
            checkFormFields( fields );
        });

        // Check validate for all fields
        cdata_email.on('focusout', function() {
            checkFormFields( fields );
        });

        // Check validate for all fields
        // Important usage 2 handler, the keydown listener main functionality and focusout - check full fill the field,
        // it would be string with length equal 4 numbers only, if less - the field fill incorrect
        cdata_time.on('keydown focusout', function() {
            checkFormFields( fields );
        });

        // Активация календаря
        cdata_date.datepicker({
            dayNamesMin: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
            monthNames: ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
            nextText: "Следующий",
            prevText: "Предыдущий",
            maxDate: "+61",
            minDate: "0",
            firstDay: 1,
            dateFormat: "yy-mm-dd",
            onSelect: function( dateText, inst ) {
                //console.log('dateText - ', dateText);
                //console.log('inst - ', inst);
                //console.log('getDay - ', $(this).datepicker( "getDate" ));
                if(dateText) {
                    checkFormFields( fields );
                }
            }
        });


        // Add listener for submit button of register customer on event
        submit_form.on('click', function() {
            var obj = {
                'fio': cdata_fio.val(),
                'phone': cdata_tel.val(),
                'email': cdata_email.val(),
                'date': cdata_date.val(),
                'time': cdata_time.val()
            };

            // Send data to AJAX
            saveCustomerData( obj );

            clearFields( 'clear_fields' );
        });

        // Responce listener from server by AJAX - cdata_front_ajax.js
        $('body').on('successSaveCustomerData errorSaveCustomerData connectionErrorSaveCustomerData', function( event, data ) {

            var tag_class = 'error_msg';

            if( 'successSaveCustomerData' == event.type ) tag_class = 'success_msg';

            submit_form.before('<div class="msg ' + tag_class + '">' + data.msg + '</div>');

            setTimeout(function() {
                clearFields( 'clear_msg' );
            }, 10000);
        });

        // Functional for check form field
        function checkFormFields( fields ) {
            var request = true;

            fields.each(function(index, elem) {

                if( 'cdata_time' == $(elem).attr('name') ) {
                    if( $(elem).val().length < 4 ) {
                        request = false;
                    }
                } else {
                    if( '' == $(elem).val() ) request = false;
                }
            });

            if( 'true' == request ) request = false;

            if( request ) {
                submit_form.removeAttr('disabled');
            } else {
                submit_form.attr('disabled', 'disabled');
            }
        }

        // Functional for clear form field
        function clearFields( param ) {

            if( 'clear_fields' == param ) {
                $.each(fields, function(index, elem) {
                    $(elem).val('');
                });
            }

            if( 'clear_msg' == param ) {
                // Remove all messages
                $('#cdata_form').find('.msg').remove();
            }


            // Disable submit button
            checkFormFields( fields );

        }

        // Functional for validate form field
        /*
         * obj = {'elem':cdata_fio, 'size':29, 'reg':/[^a-z|0-9|@|\.|\-|_]/i}
         * obj.elem[jQuery object] - pass to function selector which would be validate
         * obj.size[integer] - Max length for input field
         */
        function validateField( obj ) {
            obj.elem.on('keypress', function(e) {
                var str = parseFloat($(this).val()) + '';

                // Clear field first and last name if input wrong symbol
                if( obj.reg != '' && $(this).val().search(obj.reg) != -1 ) {
                    $(this).val('');
                    if($(this).attr('name') == 'cdata_fio') {
                        $(this).after('<span class="error_fio">Пожалуйста, напишите ФИО на русском языке.</span>')
                    }

                    return false;
                } else {
                    if($(this).closest('#cdata_form').find('.error_fio').length != 0) {
                        $(this).closest('#cdata_form').find('.error_fio').remove();
                        checkFormFields( fields );
                    }
                }

                // Clear field email if input wrong symbol
                if($(this).attr('name') == 'cdata_email') {
                    if($(this).val().search(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{1,6}$/i) == -1) {
                        if($(this).closest('#cdata_form').find('.error_email').length == 0) {
                            $(this).after('<span class="error_email">Пожалуйста, укажите правильный почтовый адрес.</span>')
                            checkFormFields( fields );
                        }
                    } else {
                        $(this).closest('#cdata_form').find('.error_email').remove();
                    }
                }

                // Ограничение по количеству вводимых символов
                if( $(this).val().length > obj.size - 1) {
                    $(this).val( $(this).val().substring(0,obj.size - 1) );
                }
            });
        }
    }

});