
jQuery('body').ready(function($) {

    'use strict';

    $('a.import').on('click', function() {

        if( $('.upload_form').hasClass('hide') ) {
            $('.upload_form').removeClass('hide');
        }
    });

    var trash_notice = localStorage.getItem('trash_notice'),
        notice = $('.notice');

    if( trash_notice ) {
        if( 'successTrash' == trash_notice ) {
            notice = $('.updated.notice');
        }
        if( 'errorTrash' == trash_notice ) {
            notice = $('.error.notice');
        }

        notice.removeClass('hide');
        setTimeout(function() {
            notice.addClass('hide');
            localStorage.removeItem('trash_notice');
        }, 10000);
    }
});