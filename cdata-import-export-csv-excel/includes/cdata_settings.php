<?php
// Main admin setting page

if(!defined( 'ABSPATH' )) exit;

if( isset( $_POST['main_settings'] ) && 'update' == $_POST['main_settings'] ) {
    global $cdata_model;

    $settings_data = array(
        'cdata_settings_to' => sanitize_text_field( $_POST['cdata_settings_to'] ),
    );

    $args = array('label' => 'cdata_main_settings', 'value' => serialize( $settings_data ));

    $setting_status = $cdata_model->update_option( $args );

    if( $setting_status ) { ?>
        <div class="updated notice">
            <p><?php _e( 'Main settings successfully updated!', CDATA_TO_CSV_EXCEL_T_DOMAIN ); ?></p>
        </div>
    <?php } else { ?>
        <div class="error notice">
            <p><?php _e( 'Main settings didn`t updated!', CDATA_TO_CSV_EXCEL_T_DOMAIN ); ?></p>
        </div>
    <?php }
}

if( isset( $_POST['form_settings'] ) && 'update' == $_POST['form_settings'] ) {
    global $cdata_model;

    $settings_data = array(
        'cdata_settings_form_title' => sanitize_text_field( $_POST['cdata_settings_form_title'] ),
        'cdata_settings_fio' => sanitize_text_field( $_POST['cdata_settings_fio'] ),
        'cdata_settings_phone' => sanitize_text_field( $_POST['cdata_settings_phone'] ),
        'cdata_settings_email' => sanitize_email( $_POST['cdata_settings_email'] ),
        'cdata_settings_data' => sanitize_text_field( $_POST['cdata_settings_data'] ),
        'cdata_settings_time' => sanitize_text_field( $_POST['cdata_settings_time'] ),
        'cdata_settings_submit_btn' => sanitize_text_field( $_POST['cdata_settings_submit_btn'] ),
    );

    $args = array('label' => 'cdata_form_settings', 'value' => serialize( $settings_data ));

    $setting_status = $cdata_model->update_option( $args );

    if( $setting_status ) { ?>
        <div class="updated notice">
            <p><?php _e( 'Form settings successfully updated!', CDATA_TO_CSV_EXCEL_T_DOMAIN ); ?></p>
        </div>
    <?php } else { ?>
        <div class="error notice">
            <p><?php _e( 'Form settings didn`t updated!', CDATA_TO_CSV_EXCEL_T_DOMAIN ); ?></p>
        </div>
    <?php }
}

$main_settings = unserialize( get_option('cdata_main_settings') );
$form_settings = unserialize( get_option('cdata_form_settings') );

global $cdata_controller;
echo $cdata_controller->admin_get_sittings( $main_settings, $form_settings );