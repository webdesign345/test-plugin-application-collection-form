<?php

if(!defined( 'ABSPATH' )) exit;

if( !class_exists('Cdata_Menu')) {

    class Cdata_Menu {

        // Initialize main plugin functional
        public function __construct() {

            global $cdata_plugin;
            add_filter("plugin_action_links_$cdata_plugin", array(__CLASS__, 'cdata_more_plugin_link') );

            add_action('admin_enqueue_scripts', array(&$this, 'cdata_to_csv_excel_scripts'));

            if (is_admin()) {
                add_action('admin_menu', array(&$this, 'cdata_to_csv_excel_admin_menu'));
            } else {
                add_action('wp_enqueue_scripts', array(&$this, 'cdata_front_scripts') );
            }
        }

        // Add scrips and styles
        public function cdata_to_csv_excel_scripts() {

        }

        // Menu item functional
        public function cdata_to_csv_excel_admin_menu() {
            $page_txt = __('Customer data to csv/excel', CDATA_TO_CSV_EXCEL_T_DOMAIN);
            $menu_title_txt = __('Cdata to csv excel', CDATA_TO_CSV_EXCEL_T_DOMAIN);
            $parent_slug = 'cdata_to_csv_excel';
            $main_menu = add_menu_page($page_txt, $menu_title_txt, 'manage_options', $parent_slug, array(&$this, 'cdata_to_csv_excel_get_menu'), 'dashicons-update-alt', 48.6);

            $lang_txt = __('Settings', CDATA_TO_CSV_EXCEL_T_DOMAIN);
            $settings = add_submenu_page($parent_slug, $lang_txt, $lang_txt, 'manage_options', 'cdata_to_csv_excel-settings', array(&$this, 'cdata_to_csv_excel_get_menu'));

            // Add scripts and styles to plugin menu page
            add_action('admin_print_scripts-' . $main_menu, array(&$this, 'cdata_to_csv_excel_main_menu_scripts') );
            add_action('admin_print_scripts-' . $settings, array(&$this, 'cdata_to_csv_excel_settings_scripts') );
        }

        // Break functional on files
        public function cdata_to_csv_excel_get_menu(){
            $current_page = isset($_REQUEST['page']) ? sanitize_text_field($_REQUEST['page']) : 'cdata_to_csv_excel';
            switch ($current_page) {
                case 'cdata_to_csv_excel-settings':
                    include('cdata_settings.php');
                    break;
                default : include('cdata-main-page.php');
            }
        }

        // Add scripts and styles on main menu page
        public function cdata_to_csv_excel_main_menu_scripts() {
            wp_enqueue_style('cdata-admin-main-menu', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/css/cdata_admin_main_menu.css', false, null);

            wp_enqueue_script('cdata-admin-main', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/js/cdata-admin-main.js');
        }

        // Add scripts and styles on setting page
        public function cdata_to_csv_excel_settings_scripts() {
            wp_enqueue_style('cdata-admin-settings', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/css/cdata_admin_settings.css', false, null);
        }

        public function cdata_front_scripts() {
            wp_enqueue_style('jqueryui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css', false, null);
            wp_enqueue_style('cdata-front', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/css/cdata_front.css', false, null);

            wp_enqueue_script('jquery-ui-datepicker');

            wp_register_script( 'cdata_front_ajax', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/js/cdata_front_ajax.js', array('jquery'), null, 'in_footer');
            wp_enqueue_script( 'cdata_front_ajax' );
            wp_localize_script( 'cdata_front_ajax', 'cdata_ajax_object', array( 'url' => admin_url( 'admin-ajax.php' ), 'nonce' => wp_create_nonce('cdata_ajax_object') ) );

            wp_enqueue_script('cdata-front-mask-input', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/js/jquery.maskedinput.min.js', array('jquery'), null, 'in_footer');
            wp_enqueue_script('cdata-front', CUSTOMER_DATA_TO_CSV_EXCEL_URL . 'assets/js/front_script.js', array('jquery' ), null, 'in_footer');
        }

        // Add settings link on plugin page
        public static function cdata_more_plugin_link( $links ) {

            $settings_link = '<a href="/wp-admin/admin.php?page=cdata_to_csv_excel-settings" title="'
                . __('Visit plugin page', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '">' . __("Settings", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a>';

            array_unshift($links, $settings_link);

            return $links;
        }
    }
}

global $cdata_menu;
$cdata_menu = new Cdata_Menu();