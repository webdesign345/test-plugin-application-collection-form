<?php

if(!defined( 'ABSPATH' )) exit;

if( !class_exists('Cdata_Model')) {
    class Cdata_Model {

        public function __construct() {
            add_action('cdata_insert_default_data', array(&$this, 'install_default_data'));
            add_action('cdata_insert_default_data', array(&$this, 'install_default_settings'));
        }

        public function install_default_data() {
            $attr = array(
                array( 'fio' => __( 'Анатолий Захарчук', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(900)-898-55-33', 'email' => 'ant@gmail.com', 'date' => '2020-06-12', 'time' => '12:15' ),
                array( 'fio' => __( 'Вероника Бойцова', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(911)-825-55-33', 'email' => 'vka_@mail.ru', 'date' => '2020-06-03', 'time' => '10:15' ),
                array( 'fio' => __( 'Дмитрий Красников', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(911)-780-55-33', 'email' => 'dk_@mail.ru', 'date' => '2020-08-01', 'time' => '15:00' ),
                array( 'fio' => __( 'Валений Беспалый', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(911)-710-25-33', 'email' => 'vds@test.ru', 'date' => '2020-08-28', 'time' => '10:45' ),
                array( 'fio' => __( 'Троофим Абдулаевич', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(900)-898-55-33', 'email' => 'ant@gmail.com', 'date' => '2020-07-18', 'time' => '12:15' ),
                array( 'fio' => __( 'Василий Кроликов', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(965)-825-40-33', 'email' => 'vka_@mail.ru', 'date' => '2020-06-30', 'time' => '10:15' ),
                array( 'fio' => __( 'Кирилл Пузанов', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(930)-100-55-33', 'email' => 'dk_@info.ru', 'date' => '2020-07-09', 'time' => '16:00' ),
                array( 'fio' => __( 'Вячеслав Кривошеев', CDATA_TO_CSV_EXCEL_T_DOMAIN ), 'phone' => '+7(980)-710-98-11', 'email' => 'dsaf@site.ru', 'date' => '2020-08-14', 'time' => '17:45' ),
            );

            $this->truncate_table();

            foreach($attr as $item) {
                $this->save_customer_data( $item );
            }
        }

        public function install_default_settings() {

            $main_settings_data = array(
                'cdata_settings_to' => '',
            );

            $args = array('label' => 'cdata_main_settings', 'value' => serialize( $main_settings_data ));

            $this->update_option( $args );

            $form_settings_data = array(
                'cdata_settings_form_title' => __( 'Leave a request for presentation', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_fio'        => __( 'Input your first name and last name', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_phone'      => __( 'Input your phone number, for example +7(900) 999-99-99', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_email'      => __( 'Input your email address, for example john123@gmail.com', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_data'       => __( 'Select presentation date', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_time'       => __( 'Select presentation time', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
                'cdata_settings_submit_btn' => __( 'Submit', CDATA_TO_CSV_EXCEL_T_DOMAIN ),
            );

            $args = array('label' => 'cdata_form_settings', 'value' => serialize( $form_settings_data ));

            $this->update_option( $args );

        }

        public function update_option( $args ) {
            return update_option( $args['label'], $args['value'] );
        }

        public function truncate_table () {
            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            $wpdb->query("TRUNCATE TABLE $table");
        }

        public function save_customer_data( $attr ) {
            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            // $attr - is clearing with sanitize function
            $result = $wpdb->insert($table, array( 'cdata_fio' => $attr['fio'], 'cdata_phone' => $attr['phone'], 'cdata_email' => $attr['email'], 'cdata_date' => $attr['date'], 'cdata_time' => $attr['time'] ), array('%s', '%s', '%s', '%s', '%s'));

            return $result;
        }

        public function update_customer_data( $attr ) {
            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            // $attr - is clearing with sanitize function
            // If data was exists query try to update it
            // If data not exist query create new data
            $result = $wpdb->replace($table, array( 'cdata_fio' => $attr['fio'], 'cdata_phone' => $attr['phone'], 'cdata_email' => $attr['email'], 'cdata_date' => $attr['date'], 'cdata_time' => $attr['time'] ), array('%s', '%s', '%s', '%s', '%s'));

            return $result;
        }

        public function get_customers_data( $cdata_orderby, $cdata_order ) {
            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            // $cdata_orderby - fio
            // $cdata_orderby - phone
            // $cdata_orderby - email
            // $cdata_orderby - date
            // $cdata_orderby - time
            $orderby = 'cdata_date';
            if( !empty( $cdata_orderby ) ) {
                $orderby = $cdata_orderby;
            }

            $result = $wpdb->get_results( "SELECT * FROM $table ORDER BY $orderby $cdata_order" );

            return $result;
        }

        public function remove_customer_data( $param ) {
            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            $result = $wpdb->delete($table, array( 'cdata_id' => $param['id']));

            return $result;
        }

        public function cdata_export() {

            global $wpdb;

            $table = $wpdb->prefix . 'cdata_to_csv_excel';

            $results = $wpdb->get_results( "SELECT * FROM $table", ARRAY_A );

            $responce = array();

            if( count($results) > 0 ){

                $responce['responce_db'] = true;
                $responce['results'] = $results;

            } else {
                $responce['responce_db'] = false;
                $responce['results'] = __('Connection to Database error, results not found', CDATA_TO_CSV_EXCEL_T_DOMAIN);
            }

            return $responce;
        }
    }
}

global $cdata_model;
$cdata_model = new Cdata_Model();