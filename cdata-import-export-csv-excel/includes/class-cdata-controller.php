<?php

if(!defined( 'ABSPATH' )) exit;

if( !class_exists('Cdata_Controller')) {
    class Cdata_Controller {

        public function __construct() {

            // Call to action with callback function
            add_action('cdata_form_send_mail', array( &$this, 'cdata_form_send_mail_callback' ) );

            // Call action to save important data to log file
            add_action('cdata_save_log', array( &$this, 'cdata_save_log_callback'), 10, 3 );

            // Call action to export data with AJAX
            add_action('wp_ajax_cdata_export', array( &$this, 'cdata_export_callback' ), 10 );

            // Call action to trash customer data
            add_action('wp_ajax_cdata_trash', array( &$this, 'cdata_trash_callback' ), 10 );

            // Allow to load on wp admin file extensions
            add_filter( 'upload_mimes', array(&$this, 'cdata_upload_allow_types'), 999 );

            // Call action to delete import files
            add_action('cdata_delete_import_file', array(&$this, 'cdata_delete_import_file_callback'), 10);

            // Catch AJAX form data
            add_action( 'wp_ajax_saveCustomerData', array( &$this, 'save_customer_data_callBack' ), 999 );
            add_action( 'wp_ajax_nopriv_saveCustomerData', array( &$this, 'save_customer_data_callBack' ), 999 );
        }

        public function save_customer_data_callBack() {

            if (wp_verify_nonce($_POST['nonce'], 'cdata_ajax_object')) {

                $attr = array(
                    'fio' => sanitize_text_field($_REQUEST['fio']),
                    'phone' => sanitize_text_field($_REQUEST['phone']),
                    'email' => sanitize_email($_REQUEST['email']),
                    'date' => sanitize_text_field($_REQUEST['date']),
                    'time' => sanitize_text_field($_REQUEST['time']),
                );

                global $cdata_model;
                $responce = $cdata_model->save_customer_data( $attr );

                $message = '';

                if( false === $responce ) {
                    $message = __('Application failed to send. Try again later or contact your <a href="mailto:' . get_option("admin_email") . '">administrator</a> to resolve this issue.', CDATA_TO_CSV_EXCEL_T_DOMAIN );
                } else {

                    // Start action for sending message to site admin
                    do_action('cdata_form_send_mail', $attr );

                    $message = __('Application sent successfully. An administrator will contact you to agree on the details.', CDATA_TO_CSV_EXCEL_T_DOMAIN );
                }

                wp_send_json_success(array('responce' => $responce, 'responce_note' => 'return number if success, or return false', 'msg' => $message ));
            } else {
                wp_send_json_error(array('nonce_sending'=>$_POST['nonce'], 'nonce_server'=>wp_create_nonce('cdata_ajax_object'), 'msg'=>__('Connection error. Try again later or contact your <a href="mailto:' . get_option("admin_email") . '">administrator</a> to resolve this issue.', CDATA_TO_CSV_EXCEL_T_DOMAIN)));
            }
        }

        /*
         * $attr - array()
         * $attr['fio'] - customer fio
         * $attr['phone'] - customer phone
         * $attr['email'] - customer email
         * $attr['date'] - date to presentation
         * $attr['time'] - time to presentation
         */
        public function cdata_form_send_mail_callback( $attr ) {

            $main_settings = unserialize( get_option('cdata_main_settings') );
            $to = ( ! empty( $main_settings['cdata_settings_to'] ) ) ? $main_settings['cdata_settings_to'] : get_option('admin_email');

            $mail_subject = __('Application for presentation from - ' . $attr['fio'], CDATA_TO_CSV_EXCEL_T_DOMAIN);

            $mail_text = '';

            $mail_text .= __( 'You have application for presentation from customer - ' . $attr['fio'], CDATA_TO_CSV_EXCEL_T_DOMAIN ) . "\r\n";
            $mail_text .= __( 'Customer data:', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . "\r\n";
            $mail_text .= __( ' - phone - ' . $attr['phone'], CDATA_TO_CSV_EXCEL_T_DOMAIN ) . "\r\n";
            $mail_text .= __( ' - email - ' . $attr['email'], CDATA_TO_CSV_EXCEL_T_DOMAIN ) . "\r\n";

            $mail_text .= __( 'The customer set the date ' . $attr['date'] . ' and time ' . $attr['time'] . ' of the presentation', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . "\r\n";

            $headers = '"From: ' . get_option('blogname') . ' <wordpress@site.ru>"' . "\r\n";
            $headers .= '"MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type: text/html; charset=utf-8 \r\n";

            $cdata_settings_email = get_option('cdata_settings_email');
            if( ! empty( $cdata_settings_email ) ) {
                $headers .= '"Cc:' . $cdata_settings_email . '"' . "\r\n";
            }

            $status = wp_mail($to, $mail_subject, $mail_text, $headers );

            $mess = '';

            if( ! $status ) {

                $data = serialize( array(
                    'to' => $to,
                    'subject' => $mail_subject,
                    'text' => $mail_text,
                    'headers' => $headers,
                ) );

                $mess = __('The message was not send - ' . $data, CDATA_TO_CSV_EXCEL_T_DOMAIN);

                // Save error message to log file
                do_action('cdata_save_log', 'message - ', $mess, 'sending_email_error');
            }

        }

        /*
         * $param1 - message key
         * $param2 - message value
         * $param3 - part of log file, for example '/' . $param3 . '.log'
         */
        public function cdata_save_log_callback( $param1, $param2, $param3 ) {

            $fileOpen = fopen ( __DIR__ . '/' . $param3 . '.log', 'a' );
            if ( $fileOpen ) {
                $fileWrite = fwrite ( $fileOpen, '######################### START ###################' . PHP_EOL );
                if ( $fileWrite ) {
                    fwrite ( $fileOpen, 'date - ' . date('d-m-Y H:i:s') . PHP_EOL );
                    fwrite ( $fileOpen, $param1 . $param2 . PHP_EOL );
                    fwrite ( $fileOpen, '##################################### END ####################################' . PHP_EOL );
                    fclose ( $fileOpen );
                } else {
                    echo '<script>alert("Error for writing log file");</script>';
                }

            } else {
                echo '<script>alert("Error for opening log file");</script>';
            }
        }

        public function  cdata_export_callback() {

            global $cdata_model;

            /*
             * $responce - array(),
             * $responce['responce_db'] - boolean, true if connection success and results not null, false - ;
             * $responce['results'] - db results if success and error string if false
             */
            $responce = $cdata_model->cdata_export();

            if( $responce['responce_db'] ) {

                $csv_output = '';

                $results = $responce['results'];

                foreach( $results as $key => $result ){

                    // Create table header from database table row title
                    if( 0 == $key ) {
                        $data_key = array_keys( $result );
                        $str = implode( ",", $data_key );
                        $str = str_replace( 'cdata_', '', $str );
                        $csv_output .= $str . "\n";
                    }

                    // Revert date format from yyyy-mm-dd to dd-mm-yyyy
                    // Add settings to same page
                    $result['cdata_date'] = $this->revert_date( $result['cdata_date'] );

                    $result = array_values( $result );
                    $result = implode( ",", $result );
                    $csv_output .= $result . "\n";
                }

                $export_format = sanitize_text_field( $_REQUEST['format'] );
                $file_name = $export_format . '_export';

                // Get settings
                $cdata_settings_csv_export_filename = get_option('cdata_settings_csv_export_filename');
                if( ! empty( $cdata_settings_csv_export_filename ) ) {
                    $file_name = $cdata_settings_csv_export_filename;
                }

                $full_filename = $file_name . '_' .date("Y-m-d_H-i",time());

                header( "Content-type: application/vnd.ms-excel" );
                header( "Content-disposition: " . $export_format . date("Y-m-d") . "." . $export_format );
                header( "Content-disposition: filename=" . $full_filename . "." . $export_format );

                echo $csv_output;

            } else {

                $message = __( 'Export data was fail', CDATA_TO_CSV_EXCEL_T_DOMAIN );

                // Save error message to log file
                do_action('cdata_save_log', 'message - ', $message, 'csv_export_error');

                echo $message;
            }

            exit;
        }

        public function cdata_trash_callback() {
            global $cdata_model;

            $param = array(
                'action' => $_REQUEST['action'],
                'id' => $_REQUEST['cdata_id']
            );

            $trash_response = $cdata_model->remove_customer_data( $param );

            if( 0 != $trash_response ) {
                echo '<script>localStorage.setItem("trash_notice", "successTrash");</script>';
            } else {
                echo '<script>localStorage.setItem("trash_notice", "errorTrash");</script>';
            }

            echo '<script>location.href = location.origin + "/wp-admin/admin.php?page=cdata_to_csv_excel";</script>';

        }

        public function cdata_read_import_file( $file ) {

            global $cdata_model;

            $file_data = file ( $file['url'], FILE_IGNORE_NEW_LINES );

            $key_arr = array( 'fio', 'phone', 'email', 'date', 'time', );

            if( $file_data ) {
                foreach($file_data as $key_file => $file_row) {
                    if( 0 == $key_file ) continue;

                    $file_row = trim( $file_row );
                    $row_arr = explode( ',', $file_row );

                    // Remove first item
                    array_shift( $row_arr );

                    // Revert date format from dd-mm-yyyy to yyyy-mm-dd
                    $row_arr[3] = $this->revert_date( $row_arr[3] );

                    // Combine array like key with array like value
                    $row_data = array_combine( $key_arr, $row_arr );

                    $fileOpen = fopen ( __DIR__ . "/logs.log", 'a' );
                    if ( $fileOpen ) {
                        $fileWrite = fwrite ( $fileOpen, '######################### START ###################' . PHP_EOL );
                        if ( $fileWrite ) {
                            fwrite ( $fileOpen, 'date - ' . date('d-m-Y H:i:s') . PHP_EOL );
                            fwrite ( $fileOpen, '$key_arr - ' . serialize($key_arr) . PHP_EOL );
                            fwrite ( $fileOpen, '$row_arr - ' . serialize($row_arr) . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data - ' . serialize($row_data) . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data[fio] - ' . $row_data['fio'] . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data[phone] - ' . $row_data['phone'] . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data[email] - ' . $row_data['email'] . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data[date] - ' . $row_data['date'] . PHP_EOL );
                            fwrite ( $fileOpen, '$row_data[time] - ' . $row_data['time'] . PHP_EOL );
                            fwrite ( $fileOpen, '##################################### END ####################################' . PHP_EOL );
                            fclose ( $fileOpen );
                        } else {
                            echo '<script>alert("Error for writing file");</script>';
                        }

                    } else {
                        echo '<script>alert("Error for opening file");</script>';
                    }

                    // Update database
                    // Return number of updated row or 0 if data is equal each other
                    $result = $cdata_model->update_customer_data( $row_data );

                    if( false == $result) {

                        $message = __('Update database was fail', CDATA_TO_CSV_EXCEL_T_DOMAIN);

                        // Save error message to log file
                        do_action('cdata_save_log', 'message - ', $message, 'update_db_error');
                    }
                }

                // After import data the file must be deleted
                do_action('cdata_delete_import_file', $file);
            } else {
                $message = __('Error read file', CDATA_TO_CSV_EXCEL_T_DOMAIN);

                // Save error message to log file
                do_action('cdata_save_log', 'message - ', $message, 'read_file_error');
            }
        }

        public function revert_date( $date ) {
            return implode('-', array_reverse( explode( '-', $date ) ) );
        }

        public function cdata_delete_import_file_callback( $file ) {
            wp_delete_file( $file['file'] );
        }

        public function front_get_form() {
            $settings = unserialize( get_option('cdata_form_settings') );

            $str = '<form id="cdata_form" method="post" onsubmit="return false;">
                        <div class="str ta_center title">' . $settings['cdata_settings_form_title'] . '</div>
                        <div class="str">
                            <label for="cdata_fio">' . $settings['cdata_settings_fio'] . '</label>
                            <input id="cdata_fio" name="cdata_fio" value="" type="text"/>
                        </div>
                        <div class="str str50">
                            <div class="block">
                                <label for="cdata_tel">' . $settings['cdata_settings_phone'] . '</label>
                                <input id="cdata_tel" name="cdata_tel" value="" type="tel"/>
                            </div>
                            <div class="block">
                                <label for="cdata_email">' . $settings['cdata_settings_email'] . '</label>
                                <input id="cdata_email" name="cdata_email" value="" type="email"/>
                            </div>
                        </div>
                        <div class="str str50">
                            <div class="block">
                                <label for="cdata_date">' . $settings['cdata_settings_data'] . '</label>
                                <input id="cdata_date" name="cdata_date" value="" type="date"/>
                            </div>
                            <div class="block">
                                <label for="cdata_time">' . $settings['cdata_settings_time'] . '</label>
                                <input id="cdata_time" name="cdata_time" value="" type="time"/>
                            </div>
                        </div>
                        <div class="str">
                            <input id="cdata_submit" name="cdata_submit" value="' . $settings['cdata_settings_submit_btn'] . '" type="submit" disabled="disabled"/>
                        </div>
                    </form>';

            return $str;
        }

        public function admin_get_sittings( $main_settings, $form_settings ) {
            $str = '';

            $str = '<div class="wrapper">
                    <h1 class="title">' . __('Main settings', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</h1>
                    <form method="post">
                        <input type="hidden" name="main_settings" value="update" />
                        <div class="str">
                            <label for="cdata_settings_to">' . __( 'Input email for sending mail', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_to" type="text" class="cdata_settings_to" name="cdata_settings_to" value="' . $main_settings['cdata_settings_to'] . '" />
                        </div>
                        <div class="str">
                            <input id="main_submit_btn" class="main_submit_btn" type="submit" name="main_submit" value="' . __( 'Save main settings', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '" />
                        </div>
                    </form>
                </div>
                <div class="wrapper">
                    <h1 class="title">' . __('Form settings', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</h1>
                    <form method="post">
                        <input type="hidden" name="form_settings" value="update" />
                        <div class="str">
                            <label for="cdata_settings_form_title">' . __( 'Input text for fio label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_form_title" type="text" class="cdata_settings_form_title" name="cdata_settings_form_title" value="' . $form_settings['cdata_settings_form_title'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_fio">' . __( 'Input text for fio label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_fio" type="text" class="cdata_settings_fio" name="cdata_settings_fio" value="' . $form_settings['cdata_settings_fio'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_phone">' . __( 'Input text for phone label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_phone" type="text" class="cdata_settings_phone" name="cdata_settings_phone" value="' . $form_settings['cdata_settings_phone'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_email">' . __( 'Input text for email label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_email" type="text" class="cdata_settings_email" name="cdata_settings_email" value="' . $form_settings['cdata_settings_email'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_data">' . __( 'Input text for date label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_data" type="text" class="cdata_settings_data" name="cdata_settings_data" value="' . $form_settings['cdata_settings_data'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_time">' . __( 'Input text for time label field', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_time" type="text" class="cdata_settings_time" name="cdata_settings_time" value="' . $form_settings['cdata_settings_time'] . '" />
                        </div>
                        <div class="str">
                            <label for="cdata_settings_submit_btn">' . __( 'Input text for submit button', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</label>
                            <input id="cdata_settings_submit_btn" type="text" class="cdata_settings_submit_btn" name="cdata_settings_submit_btn" value="' . $form_settings['cdata_settings_submit_btn'] . '" />
                        </div>
                        <div class="str">
                            <input id="submit_btn" class="submit_btn" type="submit" name="form_submit" value="' . __( 'Save form settings', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '" />
                        </div>
                    </form>
                </div>';

            return $str;
        }

        public function admin_get_customers_data( $cdata_orderby, $cdata_order) {

            global $cdata_model;

            if( empty($cdata_orderby) ) $cdata_orderby = 'cdata_date';
            if( empty($cdata_order) ) $cdata_order = 'ASC';

            $responce = $cdata_model->get_customers_data( $cdata_orderby, $cdata_order );

            $cdata_fio_order = $cdata_phone_order = $cdata_email_order = $cdata_date_order = $cdata_time_order = 'DESC';
            $cdata_fio_order_pointer = $cdata_phone_order_pointer = $cdata_email_order_pointer = $cdata_date_order_pointer = $cdata_time_order_pointer = 'dashicons-arrow-up';

            if( $cdata_order == 'DESC' ) {

                switch( $cdata_orderby ) {
                    case 'cdata_fio' :      $cdata_fio_order = 'ASC';
                        $cdata_fio_order_pointer = 'active dashicons-arrow-down';
                        break;

                    case 'cdata_phone' : $cdata_phone_order = 'ASC';
                        $cdata_phone_order_pointer = 'active dashicons-arrow-down';
                        break;

                    case 'cdata_email' : $cdata_email_order = 'ASC';
                        $cdata_email_order_pointer = 'active dashicons-arrow-down';
                        break;

                    case 'cdata_date' : $cdata_date_order = 'ASC';
                        $cdata_date_order_pointer = 'active dashicons-arrow-down';
                        break;

                    case 'cdata_time' : $cdata_time_order = 'ASC';
                        $cdata_time_order_pointer = 'active dashicons-arrow-down';
                        break;
                }
            } else {
                switch( $cdata_orderby ) {
                    case 'cdata_fio' : $cdata_fio_order_pointer = 'active dashicons-arrow-up';
                        break;

                    case 'cdata_phone' : $cdata_phone_order_pointer = 'active dashicons-arrow-up';
                        break;

                    case 'cdata_email' : $cdata_email_order_pointer = 'active dashicons-arrow-up';
                        break;

                    case 'cdata_date' : $cdata_date_order_pointer = 'active dashicons-arrow-up';
                        break;

                    case 'cdata_time' : $cdata_time_order_pointer = 'active dashicons-arrow-up';
                        break;
                }
            }

            $str = '';

            if( !empty( $responce ) ) {
                $str = '<div class="wrap">
                    <h1 class="wp-heading-inline">' . __('Customer data', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</h1>
                    <a class="page-title-action export export_csv" href="/wp-admin/admin-ajax.php?action=cdata_export&format=csv">' . __('Export to CSV', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a>
                    <a class="page-title-action export export_xls" href="/wp-admin/admin-ajax.php?action=cdata_export&format=xls">' . __('Export to EXCEL', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a>
                    <span class="divider">/</span>
                    <a class="page-title-action import import_csv" href="javascript:void(0);">' . __('Import from CSV', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a>
                    <a class="page-title-action import import_xls hide href="javascript:void(0);">' . __('Import from EXCEL', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a>
                    <div class="updated notice hide">
                        <p>' . __( 'Selected customers data successfully removed!', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</p>
                    </div>
                    <div class="error notice hide">
                        <p>' . __( 'Selected customers data did not removed. In process removing was fail, try some time later or relax and drink a cap of coffee!', CDATA_TO_CSV_EXCEL_T_DOMAIN ) . '</p>
                    </div>
                    <div class="upload_form hide">
                        <form enctype="multipart/form-data" method="post">' .
                            wp_nonce_field( 'cdata_file_upload', 'fileup_nonce' ) . '
                            <input name="cdata_file_upload" type="file" />
                            <input type="submit" value="' . __('Upload file', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '" />
                        </form>
                    </div>
                    <hr class="wp-header-end"/>
                    <table class="wp-list-table widefat fixed striped posts">
                        <thead>
                            <tr>
                                <th><a href="/wp-admin/admin.php?page=cdata_to_csv_excel&orderby=cdata_fio&order=' . $cdata_fio_order . '">' . __("First name, Last Name", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '<span class="dashicons hide ' . $cdata_fio_order_pointer . '"></span></a></th>
                                <th><a href="/wp-admin/admin.php?page=cdata_to_csv_excel&orderby=cdata_phone&order=' . $cdata_phone_order . '">' . __("Phone", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '<span class="dashicons hide ' . $cdata_phone_order_pointer . '"></span></a></th>
                                <th><a href="/wp-admin/admin.php?page=cdata_to_csv_excel&orderby=cdata_email&order=' . $cdata_email_order . '">' . __("Email", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '<span class="dashicons hide ' . $cdata_email_order_pointer . '"></span></a></th>
                                <th><a href="/wp-admin/admin.php?page=cdata_to_csv_excel&orderby=cdata_date&order=' . $cdata_date_order . '">' . __("Date", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '<span class="dashicons hide ' . $cdata_date_order_pointer . '"></span></a></th>
                                <th><a href="/wp-admin/admin.php?page=cdata_to_csv_excel&orderby=cdata_time&order=' . $cdata_time_order . '">' . __("Time", CDATA_TO_CSV_EXCEL_T_DOMAIN) . '<span class="dashicons hide ' . $cdata_time_order_pointer . '"></span></a></th>
                            </tr>
                        </thead>
                        <tbody>';

                            foreach($responce as $customer_item) {

                                $fio = $customer_item->cdata_fio;
                                $phone = $customer_item->cdata_phone;
                                $email = $customer_item->cdata_email;
                                $date = $customer_item->cdata_date;
                                $time = $customer_item->cdata_time;

                                $str .= '<tr>
                                            <td>
                                                <span>' . $fio . '</span>
                                                <div class="row-actions">
                                                    <span class="trash"><a href="/wp-admin/admin-ajax.php?action=cdata_trash&cdata_id=' . $customer_item->cdata_id . '" class="submit delete" aria-label="' . __('Delete', CDATA_TO_CSV_EXCEL_T_DOMAIN) . ' «' . $fio . '»">' . __('Delete', CDATA_TO_CSV_EXCEL_T_DOMAIN) . '</a></span>
                                                </div>
                                            </td>
                                            <td>' . $phone . '</td>
                                            <td>' . $email . '</td>
                                            <td>' . $date . '</td>
                                            <td>' . $time . '</td>
                                        </tr>';
                            }

                $str .= '</tbody>
                    </table>
                </div>';
            } else {
                $str = __('<h1 class="wp-heading-inline">Sorry, customers not found!</h1>', CDATA_TO_CSV_EXCEL_T_DOMAIN);
            }

            return $str;
        }


        public function cdata_upload_allow_types( $mimes ) {

            // Allow new mime type - https://wp-kama.ru/id_8024/razreshaem-zagruzku-zapreshhennyh-tipov-fajlov.html
            // List of mime types - https://wp-kama.ru/id_8643/spisok-rasshirenij-fajlov-i-ih-mime-tipov.html
            $mimes['csv'] = 'text/csv';
            $mimes['xls'] = 'application/excel';
            $mimes['xls'] = 'application/vnd.ms-excel';
            $mimes['xls'] = 'application/x-excel';
            $mimes['xls'] = 'application/x-msexcel';
            $mimes['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

            return $mimes;
        }
    }
}

global $cdata_controller;
$cdata_controller = new Cdata_Controller();