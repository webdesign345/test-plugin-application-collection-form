<?php
global $cdata_model;
global $cdata_controller;


if( wp_verify_nonce( $_POST['fileup_nonce'], 'cdata_file_upload' ) ){

    if ( ! function_exists( 'wp_handle_upload' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }

    $file = $_FILES['cdata_file_upload'];

    $overrides = [ 'test_form' => false ];

    $load_file = wp_handle_upload( $file, $overrides );

    // Success onload file
    if ( $load_file && empty( $load_file['error'] ) ) {
        /*
         * $load_file - array
         * $load_file['file'] => sites/wptest.ru/www/wp-content/uploads/2014/06/Daft-Punk-Something-About-Us.mp3
         * $load_file['url'] => http://wptest.ru/wp-content/uploads/2014/06/Daft-Punk-Something-About-Us.mp3
         * $load_file['type'] => audio/mpeg
         *
         */

        $cdata_controller->cdata_read_import_file( $load_file );

    } else {
        // Save error message to log file
        $message = __('Error upload file, check documentation for wp_handle_upload, error - ' . $load_file['error']. ' file - ' . serialize( $file ), CDATA_TO_CSV_EXCEL_T_DOMAIN);
        do_action('cdata_save_log', 'message - ', $message, 'file_onload_error');
    }
}

$cdata_orderby = '';
$cdata_order = '';
if( isset($_REQUEST['orderby']) && isset($_REQUEST['order']) ) {
    $cdata_orderby = $_REQUEST['orderby'];
    $cdata_order = $_REQUEST['order'];
}

echo $cdata_controller->admin_get_customers_data( $cdata_orderby, $cdata_order );