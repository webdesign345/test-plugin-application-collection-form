<?php
// Admin register shortcode page

if(!defined( 'ABSPATH' )) exit;

if( !class_exists('Cdata_Shortcode') ) {
    class Cdata_Shortcode {
        public function __construct() {

            //
            add_shortcode('cdata_form', array(&$this, 'cdata_form'));
        }

        public function cdata_form() {

            global $cdata_controller;

            return $cdata_controller->front_get_form();
        }
    }
}

global $cdata_shortcode;
$cdata_shortcode = new Cdata_Shortcode();